# M.Ed.-verkefni Sverris Hrafns Steindórssonar

Hér er að finna lausnir og önnur gögn tengd styttri verkefnum sem eru hluti af lokaverkefni til M.Ed.-prófs.

Allur kóði er undir 3ja ákvæða BSD-leyfi (e. The 3-Clause BSD License) (sjá LICENSE skrá).

Annað efni eru undir Creative Commons BY-SA-4.0 (sjá creativecommons.org)

<br>

<img src="http://honnunarstadall.hi.is/sites/honnunarstadall.hi.is/files/admin/Honnunarstadall/Logo/PNG/hi-logo_transparent-black-is.png" alt="HÍ-logo" style="width: 200px;"/>
