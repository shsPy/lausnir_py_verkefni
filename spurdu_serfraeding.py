from tkinter import Tk, simpledialog, messagebox

def lesa_ur_skra():
    with open('hofudborgir.txt') as skra:
        for lina in skra:
            lina = lina.rstrip('\n')
            land, borg = lina.split('/')
            heimurinn[land] = borg
            
def skrifa_i_skra(heiti_lands, heiti_borgar):
    with open('hofudborgir.txt') as skra:
        skra.write('\n' + heiti_lands + '/' + heiti_borgar)

print('Spurðu sérfræðinginn - höfuðborgir heimsins')
root = Tk()
root.withdraw()
heimurinn = {}
lesa_ur_skra()
while True:
    landa_spurning = simpledialog.askstring('Land','Skrifið heiti á landi:')

    if landa_spurning in heimurinn:
        nidurstada = heimurinn[landa_spurning]
        messagebox.showinfo('Svar', 'Höfuðborgin á/í ' + landa_spurning + ' er ' +
                            nidurstada + '!')
    else:
        ny_borg = simpledialog.askstring('Kenndu mér', 'Ég veit þetta ekki\n' +
                                         'Hver er höfuðborg ' + landa_spurning +
                                         '?')
        heimurinn[landa_spurning] = ny_borg
        skrifa_i_skra(landa_spurning, ny_borg)
root.mainloop()
